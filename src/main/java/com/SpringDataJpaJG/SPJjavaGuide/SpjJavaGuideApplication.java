package com.SpringDataJpaJG.SPJjavaGuide;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class
SpjJavaGuideApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpjJavaGuideApplication.class, args);
	}

}
