package com.SpringDataJpaJG.SPJjavaGuide.reository;

import com.SpringDataJpaJG.SPJjavaGuide.entity.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class ProductRepositoryTest {
    @Autowired
private  ProductRepository productRepository;
    @Test
    void saveMethode(){
        Product product=new Product();
        product.setName("product 1");
        product.setDescription("product 1 description");
        product.setSku("100ABC");
        product.setPrice(new BigDecimal(100));
        product.setActive(true);
        product.setImageUrl("product 1.png");

        Product savedObject=productRepository.save(product);

        System.out.println(savedObject.getId());
        System.out.println(savedObject.toString());
    }
    @Test
    void updateUsingSaveMethode(){
        long id=1L;
        Product product=productRepository.findById(id).get();

        product.setName("updated product 1");
        product.setDescription("updated product 1 desc");

        productRepository.save(product);

    }
    @Test
    void findByIdMethod(){
        long id=1L;
        Product product=productRepository.findById(id).get();

        System.out.println(product.toString());
    }

    @Test
    void saveAllMethode(){
        Product product=new Product();
        product.setName("product 2");
        product.setDescription("product 2 description");
        product.setSku("100ABCD");
        product.setPrice(new BigDecimal(200));
        product.setActive(true);
        product.setImageUrl("product 2.png");

        Product product3=new Product();
        product3.setName("product 3");
        product3.setDescription("product 3 description");
        product3.setSku("100ABCDE");
        product3.setPrice(new BigDecimal(300));
        product3.setActive(true);
        product3.setImageUrl("product 3.png");
// methode 1
        List<Product> list=new ArrayList<>();
        list.add(product);
        list.add(product3);
        productRepository.saveAll(list);

//        productRepository.saveAll(List.of(product,product3)); java 9
    }
    @Test
    void findAllMethod(){
        List<Product> list=productRepository.findAll();
        list.stream().forEach(System.out::println);
    }

    @Test
    void deleteByIdMethod(){
        long id=3L;
        productRepository.deleteById(id);
    }

    @Test
    void deleteMethod(){
        long id=2L;
        Product product=productRepository.findById(id).get();
        productRepository.delete(product);
    }

    @Test
    void deleteAllMethode() {
        productRepository.deleteAll();
    }

    @Test
    void deleteAllListMethode() {

        Product product=productRepository.findById(6L).get();
        Product product1=productRepository.findById(7L).get();
        List<Product> list=new ArrayList<>();
        list.add(product);
        list.add(product1);

        productRepository.deleteAll(list);
    }

    @Test
    void countMethode() {
       Long l= productRepository.count();
        System.out.println(l);
    }


    @Test
    void existsByIdMethod(){
        long id=10L;
        boolean b=productRepository.existsById(id);
        System.out.println(b);
    }


}